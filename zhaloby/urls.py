from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from auth.urls import auth_not_required
from django.contrib.auth.views import login
from auth.views import AboutView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', auth_not_required(login), {'template_name' : 'auth/signin.html', 'extra_context' : {'next' : '/auth/success/'}}, name='signin'),
    # url(r'^zhaloby/', include('zhaloby.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^about/', AboutView.as_view(), name='about'),
    url(r'^auth/', include('auth.urls', namespace='auth')),
    url(r'^location/', include('location.urls', namespace='location')),
    url(r'^complaint/', include('complaint.urls', namespace='complaint')),
    url(r'^category/', include('category.urls', namespace='category')),
    url(r'^comment/', include('comment.urls', namespace='comment')),
    url(r'^status/', include('status.urls', namespace='status')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^rosetta/', include('rosetta.urls')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
