from fabric.api import local, lcd, cd, sudo, prefix
from fabric.operations import run, put

APPS = ['auth', 'category', 'comment', 'complaint', 'status']

# install python and virtualenv manually on server


def init():
    with lcd("/home/darik/project"):
        local("git clone --bare zhaloby zhaloby.git")
        local("tar -cvf zhaloby.tar.gz zhaloby.git")
        put("zhaloby.tar.gz", "/home/mnenie/project")
    with cd("/home/mnenie/project"):
        run("tar -xvf zhaloby.tar.gz")
        run("git clone zhaloby.git")
        with cd("zhaloby.git/hooks"):
            run("mv post-update.sample post-update")
            run("cat /dev/null > post-update")
            run("echo '#!/bin/bash' >> post-update")
            run("echo 'cd /home/mnenie/project/zhaloby || exit' >> post-update")
            run("echo 'unset GIT_DIR' >> post-update")
            run("echo 'git pull' >> post-update")
            run("echo 'exec git-update-server-info' >> post-update")
        with cd("zhaloby"):
            run("virtualenv venv --no-site-packages")
            with prefix('source venv/bin/activate'):
                run("source venv/bin/activate")
                run("pip install -r requirements.txt")
                run("python manage.py syncdb")
                run("python manage.py migrate")
    with lcd("/home/darik/project"):
        local('rm -Rf zhaloby.git')
        local('rm zhaloby.tar.gz')


def commit():
    with lcd("/home/darik/project/zhaloby"):
        local("git add . && git commit")


def push():
    with lcd("/home/darik/project/zhaloby"):
        local("git push bitbucket master")
        local("git push production master")

    with cd('/home/mnenie/project/zhaloby'):
        with prefix('source venv/bin/activate'):
            run('pip install -r requirements.txt')
            for app in APPS:
                run('python manage.py migrate %s' % app)

    sudo('supervisorctl restart uwsgi:')
    sudo('supervisorctl restart celeryd:')