(function($){
	$(document).ready(function(e){
		$(document).on('click', "a[data-stat$='_stat']", function(e){
			e.preventDefault();
			var prefix = $(this).attr('data-stat').lastIndexOf('_');
			prefix = $(this).attr('data-stat').substring(0, prefix);
			$menu = $('#' + prefix + '_menu');
			$('#' + prefix + '_loader').css('visibility', 'visible');
			$menu.slideToggle("slow", function(){
				if ($menu.has('img').length == 0) {
					var url = '/complaint/report/' + $('#region_id').val() + '/' + prefix + '/' + $('#from').val() + '/' + $('#to').val();
					$.get(url, function(data){
						$menu.html(data);
						$('#' + prefix + '_loader').css('visibility', 'hidden');
					})
				}else {
					$('#' + prefix + '_loader').css('visibility', 'hidden');	
				}
			})
		})
	});
})(jQuery);