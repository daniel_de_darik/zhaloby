(function($){
	$(document).ready(function(e){
		$("#id_region").change(function(e){
			var region_id = $(this).val();
			if (region_id == "") region_id = 0;

			$.get("/location/cities/" + region_id, function(data){
	            if (data != "") {
	            	var $data = $(data);
	             	$('#id_city').html($data);
	            }
	        });
		});
	});
})(jQuery);