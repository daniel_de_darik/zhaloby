(function($){
	$(document).ready(function(e){
		$(document).on('click', "div[id$='_toggle']", function(e){
			var prefix = $(this).attr('id').lastIndexOf('_');
			prefix = $(this).attr('id').substring(0, prefix);
			$('#' + prefix + '_menu').slideToggle("slow", function(){
				var $arrow = $('#' + prefix + '_toggle .arrow');
				if ($arrow.hasClass('arrow-up')) {
					$arrow.removeClass('arrow-up').addClass('arrow-down');
				}else {
					$arrow.removeClass('arrow-down').addClass('arrow-up');
				}
			})
		})

		$('#scenario').change(function(e){
			var id = $(this).val();
			$.get('/status/scenario/' + id + "/", function(data){
				$('#status').html(data);
			});
		})
		
		$('#unsubscr-btn').click(function(e){
		    e.preventDefault();
		    
		    $.ajax({
			type: 'POST',
			url: '/complaint/unsubscribe/',
			data: $('#unsubscr-form').serialize(),
			
			beforeSend: function(xhr){
			    xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
			},
			success: function(json){
			    if (json.status == 'ok'){
				$('#unsubscr-div').slideUp(function(){
					$(this).remove();
				});
				$('#unsubscr-response').fadeIn("slow", function(){
					$(this).fadeOut(5000, function(){
						$(this).remove();
					});
				});
			    }
			},
			error: function(){
				alert(ERROR_MESSAGE);
			}
		    });
		})
		
		$('#comment_btn').click(function(e){
			e.preventDefault();
			
			$.ajax({
				type: 'POST',
				url: '/comment/add/',
				data: $('#comment-form').serialize(),
			
				beforeSend: function(xhr){
			    		xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
			    		$('#comment-loader').css('visibility', 'visible');
				},
				success: function(json){
					$('#comment-loader').css('visibility', 'hidden');
					if (json.status == 'ok'){
						$('#comment_menu').append(json.template);
						$('textarea#comment-text').val('');
						var commentCnt = $('#comment-cnt').text();
						$('#comment-cnt').text(parseInt(commentCnt) + 1);
					}else{
						alert('Нам не нужны пустые комментарии!');
					}
				},
				error: function(){
					$('#comment-loader').css('visibility', 'hidden');
					alert(ERROR_MESSAGE);
				}
			})
		})

		$(document).on('click', "a[show-text='hidden-text']", function(e){
			e.preventDefault();
			$(this).next().show();
			$(this).remove();
		});
		
		$('#rate').raty({
		    halfShow: true,
		    path: '/static/img/',
		    target: '#rate-target',
		    targetKeep: true,
		    score: function(){
			    return $(this).attr('data-score');
		    },
		    readOnly: function(){
			    return $(this).attr('data-readonly');
		    },
		    click: function(score, evt){
			    $('#ratio').val(score);
			    $.ajax({
				type:'POST',
				url: '/complaint/rate/',
				data: $('#rate-form').serialize(),
				beforeSend: function(xhr){
				    xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
				},
				success: function(json){
				    if (json.status == 'ok'){
					$('#rate').raty({
					    'readOnly': true,
					    'score': json.ratio,
					    'path': '/static/img/',
					});
					$('#raty-response').fadeIn("slow", function(){
					    $(this).fadeOut(5000);
					})
				    }
				},
				error: function(){
				    alert(ERROR_MESSAGE);
				}
			    });
			    //$('#rate-form').submit();
		    }
		});
		//$('#rate-target').text("");
	});
})(jQuery);