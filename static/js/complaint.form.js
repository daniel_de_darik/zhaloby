(function($){
	$(document).ready(function(e){
		$('#id_category').change(function(e){
			var categoryId = $(this).val();
			$.get('/category/sub/' + categoryId, function(data){
				$('#id_category1').html(data);
				$('#category_value_id').val(categoryId);
			})
		});

		$('#id_category1').change(function(e){
			var categoryId = $(this).val();
			$.get('/category/sub/' + categoryId, function(data){
				$('#id_category2').html(data);
				$('#category_value_id').val(categoryId);
			})
		});

		$('#id_category2').change(function(e){
			$('#category_value_id').val($(this).val());
		});
	});
})(jQuery);