from django.contrib import admin
from status.models import Status, Scenario, ScenarioStatus
# Register your models here.

admin.site.register(Status)
admin.site.register(Scenario)
admin.site.register(ScenarioStatus)