# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ScenarioStatus'
        db.create_table(u'status_scenariostatus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['status.Status'])),
            ('scenario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['status.Scenario'])),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'status', ['ScenarioStatus'])

        # Adding model 'Scenario'
        db.create_table(u'status_scenario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'status', ['Scenario'])


    def backwards(self, orm):
        # Deleting model 'ScenarioStatus'
        db.delete_table(u'status_scenariostatus')

        # Deleting model 'Scenario'
        db.delete_table(u'status_scenario')


    models = {
        u'status.scenario': {
            'Meta': {'object_name': 'Scenario'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'status.scenariostatus': {
            'Meta': {'object_name': 'ScenarioStatus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'scenario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['status.Scenario']"}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['status.Status']"})
        },
        u'status.status': {
            'Meta': {'object_name': 'Status'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['status']