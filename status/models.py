from django.db import models

# Create your models here.


class Status(models.Model):
    name = models.CharField(max_length=256)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class Scenario(models.Model):
    name = models.CharField(max_length=256)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class ScenarioStatus(models.Model):
    status = models.ForeignKey(Status)
    scenario = models.ForeignKey(Scenario)
    order = models.IntegerField(default=1)

    def __unicode__(self):
        return "%d) %s (%s)" % (self.order, self.scenario.name, self.status.name)