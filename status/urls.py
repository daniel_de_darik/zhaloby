from django.conf.urls import patterns, url
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from status import views

manager_required = user_passes_test(lambda u: u.profile.role_id >= settings.MANAGER)

urlpatterns = patterns('',
	url(r'^scenario/(?P<scenario_id>\d+)/$', manager_required(views.get_scenario_list), name='scenario_list'),
)