from django.shortcuts import render_to_response
from status.models import ScenarioStatus
from django.template import RequestContext

# Create your views here.


def get_scenario_list(request, scenario_id):
    context = RequestContext(request)
    status_list = ScenarioStatus.objects.filter(scenario_id=int(scenario_id)).order_by('order')

    return render_to_response('status/enumerate.html', {'status_list': status_list}, context)