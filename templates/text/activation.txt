Добро пожаловать, {{ full_name }}!

Вы успешно зарегестрировались на www.satisfaction.kz
Ваш логин: {{ username }}
Пароль: {{ password }}

Для активации аккаунта пройдите по www.satisfaction.kz/auth/activate/{{ activation_key }}/

С уважением,
Разработчик платформы www.satisfaction.kz