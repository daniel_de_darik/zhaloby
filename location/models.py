from django.db import models

# Create your models here.


class Region(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=10)

    def __unicode__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=50)
    region = models.ForeignKey(Region)

    def __unicode__(self):
        return self.name
