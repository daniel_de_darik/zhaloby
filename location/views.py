from django.shortcuts import render_to_response
from location.models import City
from django.template import RequestContext

# Create your views here.


def get_city_list(request, region_id):
    context = RequestContext(request)

    city_list = City.objects.filter(region_id=int(region_id)).order_by('name')

    return render_to_response('location/city_enumerate.html', {'city_list' : city_list}, context)