from django.contrib import admin
from location.models import City
from location.models import Region

# Register your models here.

admin.site.register(City)
admin.site.register(Region)
