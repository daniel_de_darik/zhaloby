from django.conf.urls import patterns, url
from location import views

urlpatterns = patterns('',
	url(r'^cities/(?P<region_id>\d+)/$', views.get_city_list, name='city_list'),
)