from django.shortcuts import render
from django.core.context_processors import csrf
from django.views.generic.base import TemplateView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from auth.forms import RegistrationForm, ProfileForm, PasswordForm
from location.models import Region
from auth.models import User, Profile
from auth.tasks import send_activation, send_reset
from django.utils.translation import ugettext as _

# Create your views here.


class AboutView(TemplateView):
    template_name = 'about.html'


def signup(request):
    # send_sms_message.delay('+77021262861', _('Your complaint has been added to our database. Thank you'))
    # send_activation.delay(1, 'whats going on?')
    if request.method == 'POST':
        reg_form = RegistrationForm(request.POST)
        profile_form = ProfileForm(request.POST)
        if reg_form.is_valid() and profile_form.is_valid():
            user = reg_form.save()
            profile_form.save(user=user)

            send_activation.delay(user.id, request.POST.get('password', None))
            # send_activation.delay(user.id, 'darkhan.imangaliev@gmail.com')

            # return HttpResponseRedirect(reverse('auth:signin'))
            return render(request, 'auth/signup_success.html', {})
    else:
        reg_form = RegistrationForm()
        profile_form = ProfileForm()

    args = {}
    args.update(csrf(request))
    args['reg_form'] = reg_form
    args['profile_form'] = profile_form
    args['region_list'] = Region.objects.all()

    return render(request, 'auth/signup.html', args)


def activate(request, key):
    try:
        profile = Profile.objects.get(activation_key=key)
        profile.user.is_active = True
        profile.user.save()
    except Profile.DoesNotExist:
        return render(request, 'auth/activate_failure.html', {})

    return render(request, 'auth/activate_success.html', {})


def reset(request):

    args = {}
    args.update(csrf(request))

    if request.method == 'POST':
        email = request.POST.get('email', None)
        try:
            user = User.objects.get(email=email)
            password = User.objects.make_random_password()
            user.set_password(password)
            user.save()

            send_reset.delay(user.id, password=password)
            return render(request, 'auth/reset_success.html')
        except User.DoesNotExist:
            args['error'] = _('Email not found')
            return render(request, 'auth/reset.html', args)

    return render(request, 'auth/reset.html', args)


def password_change(request):
    if request.method == 'POST':
        password_form = PasswordForm(request.user, request.POST)
        if password_form.is_valid():
            password_form.save()

            return render(request, 'auth/password_success.html', {})
    else:
        password_form = PasswordForm()

    args = {}
    args.update(csrf(request))
    args['form'] = password_form

    return render(request, 'auth/password_form.html', args)

