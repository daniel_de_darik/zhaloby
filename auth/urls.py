from django.conf.urls import patterns, url
from auth import views
from django.views.generic import TemplateView
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.views import login, logout_then_login

auth_not_required = user_passes_test(lambda u: not u.is_authenticated(), login_url='/auth/success/')
auth_required = user_passes_test(lambda u: u.is_authenticated(), login_url='/auth/signin/')

urlpatterns = patterns('',
	url(r'^signin/$', auth_not_required(login), {'template_name' : 'auth/signin.html', 'extra_context' : {'next' : '/auth/success/'}}, name='signin'),
	url(r'^logout/$', logout_then_login, {'login_url' : '/auth/signin/'}, name='logout'),
	url(r'^signup/$', auth_not_required(views.signup), name='signup'),
	url(r'^success/$', TemplateView.as_view(template_name='auth/signin_complete.html'), name='success'),
    url(r'^denied/$', TemplateView.as_view(template_name='auth/denied.html'), name='denied'),
    url(r'^activate/(?P<key>\w+)/$', auth_not_required(views.activate), name='activate'),
    url(r'^reset/$', auth_not_required(views.reset), name='reset'),
    url(r'^pchange/$', auth_required(views.password_change), name='pchange'),
)