from __future__ import absolute_import
from djcelery import celery
#from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
#from django.template.loader import get_template
from django.template import Context
from auth.models import User, Profile
from django.utils.translation import ugettext as _


@celery.task
def add(x, y):
    return x + y


@celery.task
def send_activation(user_id, password=None):
    try:
        profile = Profile.objects.get(user_id=user_id)
        user = profile.user

        c = Context({
                    'username': user.username,
                    'password': password,
                    'activation_key': profile.activation_key,
                    'full_name': user.first_name + " " + user.last_name
                    })

        plain_txt = render_to_string('text/activation.txt', c)
        html = render_to_string('text/activation.html', c)
        email = EmailMultiAlternatives(_('Activation of account on www.jaloby.165.kz'), plain_txt)
        email.attach_alternative(html, "text/html")
        # email.to = ['darkhan.imangaliev@gmail.com']
        email.to = [user.email]
        email.send()


        print "Email to " + user.email + " has been successfully sent"
    except Exception as e:
        print "There was an error sending email"
        print e


@celery.task
def send_reset(user_id, password=None):
    try:
        user = User.objects.get(id=user_id)
        # email = user.email

        c = Context({
                    'full_name': user.first_name + " " + user.last_name,
                    'username': user.username,
                    'password': password,
                    })

        plain_txt = render_to_string('text/reset.txt', c)
        html = render_to_string('text/reset.html', c)
        email = EmailMultiAlternatives(_('Password reset on www.jaloby.165.kz'), plain_txt)
        email.attach_alternative(html, "text/html")
        # email.to = ['darkhan.imangaliev@gmail.com']
        email.to = [user.email]
        email.send()

        print "Email to " + user.email + " has been successfully sent"
    except Exception as e:
        print "There was an error sending email"
        print e
