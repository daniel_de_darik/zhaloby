from django.db import models
from django.contrib.auth.models import UserManager
from location.models import Region, City
from django.conf import settings
import string
import random
# Create your models here.


class MyUserManager(UserManager):
    def create_user(self, username, email=None, password=None, **extra_fields):
        user = super(MyUserManager, self).create_user(username, email, password, **extra_fields)
        return user

    def reset_password(self, user):

        password = ''.join(random.choice(string.ascii_lowercase) for i in range(10))
        user.set_password(password)
        user.save()

        return password


class User(models.Model):
    email = models.EmailField(max_length=75, verbose_name="Email", blank=False, null=False)
    password = models.CharField()

    objects = MyUserManager()

    def __unicode__(self):
        return self.email

#ALTER SCHEMA `zhaloby_db`  DEFAULT CHARACTER SET utf8 ;


class ProfileManager(models.Manager):
    def create_profile(self, user, region=None, city=None):
        activation_key = ''.join(random.choice(string.ascii_lowercase) for i in range(10))
        profile = Profile(region=region, city=city, activation_key=activation_key, role_id=1)
        profile.user = user

        return profile


class Role(models.Model):
    name = models.CharField(max_length=126)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User)
    region = models.ForeignKey(Region)
    city = models.ForeignKey(City)
    role = models.ForeignKey(Role)
    activation_key = models.CharField(max_length=60)
    mobile = models.CharField(max_length=16)
    home_phone = models.CharField(max_length=16)

    objects = ProfileManager()

    def is_head(self):
        return self.role.id >= settings.HEAD

    def is_manager(self):
        return self.role.id >= settings.MANAGER

    def is_executor(self):
        return self.role.id >= settings.EXECUTOR

    def __unicode__(self):
        return self.user.username
