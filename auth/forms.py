from django import forms
from auth.models import User, Profile
from location.models import Region, City
from django.utils.translation import ugettext as _


class RegistrationForm(forms.Form):

    first_name = forms.CharField(required=True,
                                 max_length=64)

    last_name = forms.CharField(required=True,
                                max_length=64)

    username = forms.CharField(required=True,
                               max_length=20)

    email = forms.EmailField(required=True,
                             max_length=75)

    password = forms.CharField(widget=forms.PasswordInput(render_value=False),
                               min_length=5,
                               required=True)

    # class Meta:
    #     model = User
    #     fields = ('username', 'password', 'email', 'first_name', 'last_name')

    def clean_email(self):
        email = self.cleaned_data.get('email', None)
        try:
            User.objects.get(email=email)
            raise forms.ValidationError(_('Email already in use'))
        except User.DoesNotExist:
            return email

    def clean_username(self):
        username = self.cleaned_data.get('username', None)
        try:
            User.objects.get(username=username)
            raise forms.ValidationError(_('Username already in use'))
        except User.DoesNotExist:
            return username

    def save(self):
        username = self.cleaned_data.get('username', None)
        del self.cleaned_data['username']
        user = User.objects.create_user(username, **self.cleaned_data)
        user.is_active = False
        user.save()

        return user


class ProfileForm(forms.ModelForm):

    region = forms.ChoiceField(widget=forms.Select, choices=Region.objects.all().values_list('id', 'name'))
    city = forms.ChoiceField(widget=forms.Select, choices=City.objects.all().values_list('id', 'name'))

    class Meta:
        model = Profile
        fields = ('region', 'city')

    def clean_region(self):
        region_id = int(self.cleaned_data.get('region', 0))

        try:
            region = Region.objects.get(pk=region_id)
            return region
        except Region.DoesNotExist:
            raise forms.ValidationError(_('Region not found'))

    def clean_city(self):
        city_id = int(self.cleaned_data.get('city', 0))
        try:
            city = City.objects.get(pk=city_id)
            return city
        except City.DoesNotExist:
            raise forms.ValidationError(_('Region not found'))

    def save(self, **kwargs):
        user = kwargs.get('user', None)

        profile = Profile.objects.create_profile(user, **self.cleaned_data)
        profile.save()

        return profile


class PasswordForm(forms.Form):

    def __init__(self, user=None, *args, **kwargs):
        super(PasswordForm, self).__init__(*args, **kwargs)
        self.user = user

    old_password = forms.CharField(widget=forms.PasswordInput(render_value=False, attrs={'class': 'input-xlarge', 'placeholder': _('Old password')}),
                                   min_length=5,
                                   required=True)

    new_password = forms.CharField(widget=forms.PasswordInput(render_value=False, attrs={'class': 'input-xlarge', 'placeholder': _('New password')}),
                                   min_length=5,
                                   required=True)

    def clean_old_password(self):
        old_passwd = self.cleaned_data.get('old_password', None)
        if not self.user.check_password(old_passwd):
            raise forms.ValidationError(_('wrong old password'))
        return old_passwd

    def save(self):
        new_passwd = self.cleaned_data.get('new_password', None)
        self.user.set_password(new_passwd)
        self.user.save()

        return self.user