from django.contrib import admin
from auth.models import Profile, Role

# Register your models here.

admin.site.register(Profile)
admin.site.register(Role)
