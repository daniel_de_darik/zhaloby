��    ,      |      �      �  )   �  *        2     E     ]     q     �     �     �     �     �     	     '     <     L     i     �     �     �     �     �     �     �     �  '        9     G     T  #   c     �     �     �     �     �     �     �  $        ,     B     S  	   k  8   u     �  �  �  \   �  :   	     ?	  G   ]	  2   �	  [   �	  4   4
     i
      �
      �
  N   �
  N     M   f  :   �  2   �  C   "  A   f  C   �     �     �  <        M  "   e  =   �  N   �          0     J  .   h  
   �  9   �     �     �  "     ,   =  +   j  N   �  0   �  2     \   I     �  \   �  *      A user with that username already exists. Activation of account on www.jaloby.165.kz Change my password Chose a category please Chose a city please Chose a deeper category please Chose a region please City does not exist Complaint does not exist Complaint doesn't exist Different regions not allowed Don't have permission to rate Email already in use Email not found Enter a valid email address. Enter a valid username. Enter valid home phone number Enter valid mobile phone number Good Gorgeous New complaint on service New password Normal Not allowed to comment Not enough right to perform this action Not+rated+yet Old password Password reset Password reset on www.jaloby.165.kz Poor Profile does not exist Region does not exist Region not found Scenario doesn't exist Status change on Status doesn't exist Too low rights to perform the action Too short description Too short header Username already in use Very+poor Your complaint has been added to our database. Thank you wrong old password Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-08-28 16:49+0600
PO-Revision-Date: 2014-09-16 11:44+0508
Last-Translator:   <darkhan.imangaliev@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Translated-Using: django-rosetta 0.7.4
 Пользователь с таким логином уже есть в нашей базе Активация аккаунта на www.jaloby.165.kz Изменить пароль Выберите, пожалуйста, категорию жалоба Выберите, пожалуйста, город Выберите подкатегорию жалобы в подгруженном меню Выберите, пожалуйста, регион Город не найден Жалоба не найдена Жалоба не найдена У вас недостаточно прав для этого действия У вас недостаточно прав для этого действия Такой email уже зарегестрирован в нашей базе Такой e-mail не найден в нашей базе Введите коректный email адрес Введите корректное имя пользователя Неверный формат домашнего телефона Неверный формат мобильного телефона Хорошо Великолепно Поступила новая жалоба на сервис Новый пароль Удовлетворительно У вас нет прав на комментирование У вас недостаточно прав для этого действия Еще+не+оценили Старый пароль Сбросить пароль Сброс пароля на www.jaloby.165.kz Плохо Профайл пользователя не найден Регион не найден Регион не найден Сценарий не найден Смена статуса на жалобе  Статус жалобы не найден У вас недостаточно прав для этого действия Слишком короткое описание Слишком короткий заголовок Пользователь с таким логином уже есть в нашей базе Очень+плохо Ваша жалоба успешно принята на обработку. Спасибо! Неверный старый пароль 