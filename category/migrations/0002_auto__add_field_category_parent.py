# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Category.parent'
        db.add_column(u'category_category', 'parent',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['category.Category'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Category.parent'
        db.delete_column(u'category_category', 'parent_id')


    models = {
        u'category.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['category.Category']", 'null': 'True'})
        }
    }

    complete_apps = ['category']