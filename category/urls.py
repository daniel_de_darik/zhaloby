from django.conf.urls import patterns, url
from category import views
from complaint.urls import manager_required

urlpatterns = patterns('',
	url(r'^sub/(?P<category_id>\d+)/$', views.get_category_list, name='category_list'),
    url(r'^report/(?P<region_id>\d+)/(?P<frm>\d{2}-\d{2}-\d{4})/(?P<to>\d{2}-\d{2}-\d{4})/$', manager_required(views.region_report), name='region_report'),
    url(r'^report/(?P<region_id>\d+)/(?P<city_id>\d+)/(?P<frm>\d{2}-\d{2}-\d{4})/(?P<to>\d{2}-\d{2}-\d{4})/$', manager_required(views.city_report), name='city_report'),
)