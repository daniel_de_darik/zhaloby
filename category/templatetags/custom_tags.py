__author__ = 'darik'
from django import template

register = template.Library()


@register.filter(name='space2plus')
def space2plus(value):
    """converts spaces to plus sign"""
    if isinstance(value, basestring):
        return value.replace(" ", "+")
    ret = []
    for i in value:
        ret.append(i.replace(" ", "+"))
    return ret


@register.filter
def reverse(lst):
    """reverses input list"""
    return reversed(lst)

# use decorator instead
# register.filter('space2plus', space2plus)
# register.filter('reverse', reverse)
