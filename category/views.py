from django.shortcuts import render_to_response
from category.models import Category
from location.models import Region, City
from django.template import RequestContext
from django.db import connection
from datetime import datetime
# Create your views here.


def get_category_list(request, category_id):
    context = RequestContext(request)
    category_list = Category.objects.filter(parent_id=int(category_id))

    return render_to_response('category/category_enumerate.html', {'category_list': category_list}, context)


def pack(lst):
    values, category = [], []
    for i in lst:
        values.append(i[0])
        category.append(i[1])

    if len(values) == 0:
        values.append(0)
        category.append("----------")

    return [values, category]


def region_report(request, region_id, frm, to):

    region = None
    city_list = []
    try:
        region = Region.objects.get(pk=int(region_id))
        city_list = City.objects.filter(region_id=int(region_id))
    except Region.DoesNotExist:
        raise Exception("Region does not exist")

    cursor = connection.cursor()
    frm = datetime.strptime(frm, '%d-%m-%Y')
    to = datetime.strptime(to, '%d-%m-%Y')

    query = '''SELECT COUNT(cmpl.id), c.name as category_name
                      FROM complaint_complaint AS cmpl
                      LEFT JOIN category_category AS c ON c.id = cmpl.category_id
                      WHERE cmpl.date_created >= '%s'
                      AND cmpl.date_created <= '%s'
                      AND region_id = %s
                      GROUP BY cmpl.category_id
                   ''' % (frm, to, region_id)

    try:
        cursor.execute(query)
        res = cursor.fetchall()
        values, category = pack(res)
    finally:
        cursor.close()

    context = RequestContext(request)
    return render_to_response('category/report.html', {'values': values,
                                                       'categories': category,
                                                       'region': region,
                                                       'colors': ['0000FF'],
                                                       'from': frm,
                                                       'to': to,
                                                       'url': '/category/report/' + str(region.id),
                                                       'city_list': city_list,
                                                       'width': 900,
                                                       'height': 300}, context)


def city_report(request, region_id, city_id, frm, to):

    try:
        Region.objects.get(pk=int(region_id))
    except Region.DoesNotExist:
        raise Exception("Region does not exist")

    try:
        City.objects.get(pk=int(city_id))
    except City.DoesNotExist:
        raise Exception("City does not exist")

    cursor = connection.cursor()
    frm = datetime.strptime(frm, '%d-%m-%Y')
    to = datetime.strptime(to, '%d-%m-%Y')

    query = '''SELECT COUNT(cmpl.id), c.name as category_name
                      FROM complaint_complaint AS cmpl
                      LEFT JOIN category_category AS c ON c.id = cmpl.category_id
                      WHERE cmpl.date_created >= '%s'
                      AND cmpl.date_created <= '%s'
                      AND city_id = %s
                      GROUP BY cmpl.category_id
                   ''' % (frm, to, city_id)

    try:
        cursor.execute(query)
        res = cursor.fetchall()
        values, category = pack(res)
    finally:
        cursor.close()

    context = RequestContext(request)
    return render_to_response('chart.html', {'values': values,
                                             'categories': category,
                                             'colors': ['0000FF'],
                                             'width': 450,
                                             'height': 200}, context)