from django.db import models

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=256)
    description = models.TextField(blank=True)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        return self.name
