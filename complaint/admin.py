from django.contrib import admin
from complaint.models import Complaint, History, Responsible
# Register your models here.

admin.site.register(Complaint)
admin.site.register(History)
admin.site.register(Responsible)
