from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.core.context_processors import csrf
from complaint.forms import ComplaintForm, SubscribeForm, ComplaintUpdateForm, RateForm
from location.models import Region, City
from category.models import Category
from complaint.models import Complaint, Responsible, History
from auth.models import Profile
from status.models import Status, Scenario, ScenarioStatus
from comment.models import Comment
from comment.views import JSONResponseMixin
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_POST
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, FormView
from django.conf import settings
from datetime import datetime
from django.template import RequestContext
from django.db import connection
import json
from django.utils.translation import ugettext as _
# Create your views here.


class ComplaintAddView(CreateView):
    model = Complaint
    form_class = ComplaintForm
    template_name = 'complaint/form.html'

    def form_valid(self, form):
        self.complaint = form.save(user=self.request.user)
        return self.get_success_url()

    def get_success_url(self):
        return HttpResponseRedirect(reverse('complaint:view', kwargs={'pk': self.complaint.id}))

    def get_context_data(self, **kwargs):
        context = super(ComplaintAddView, self).get_context_data(**kwargs)
        context['region_list'] = Region.objects.all().order_by('name')
        context['city_list'] = City.objects.filter(region_id=self.request.user.profile.region_id).order_by('name')
        context['category_list'] = Category.objects.filter(parent_id__isnull=True)

        return context


def update(request):
    if request.method == 'POST':
        form = ComplaintUpdateForm(request.user, request.POST)
        if form.is_valid():
            complaint = form.save()
            return HttpResponseRedirect(reverse('complaint:view', kwargs={'pk': complaint.id}))
        else:
            return HttpResponseRedirect(reverse('complaint:view', kwargs={'pk': request.POST.get('complaint_id', 0)}))
    return HttpResponseRedirect(reverse('complaint:list', kwargs={'page': 1}))


@require_POST
def subscribe(request):
    if request.method == 'POST':
        form = SubscribeForm(request.POST)
        if form.is_valid():
            complaint = form.save()
            return HttpResponseRedirect(reverse('complaint:view', kwargs={'pk': complaint.id}))
        else:
            return HttpResponseRedirect(reverse('complaint:view', kwargs={'pk': request.POST.get('complaint_id', 0)}))

    return HttpResponseRedirect(reverse('complaint:my', kwargs={'page': 1}))


@require_POST
def unsubscribe(request):
    response_dict = {'status': 'error'}
    form = SubscribeForm(request.POST)
    if form.is_valid():
        form.save()
        response_dict = {'status': 'ok'}

        # return HttpResponseRedirect(reverse('complaint:view', kwargs={'pk': complaint.id}))
    # else:
    #     return HttpResponseRedirect(reverse('complaint:view', kwargs={'pk': request.POST.get('complaint_id', 0)}))
    return HttpResponse(json.dumps(response_dict), content_type='application/json')


class ComplaintDetailView(DetailView):
    model = Complaint
    context_object_name = 'complaint'
    template_name = 'complaint/view.html'

    def get_context_data(self, **kwargs):
        context = super(ComplaintDetailView, self).get_context_data(**kwargs)
        complaint = self.get_object()

        if self.request.user.profile.role_id < settings.EXECUTOR:
            if self.request.user.id != complaint.user_id:
                raise Http404

        if self.request.user.profile.role_id == settings.EXECUTOR:
            if complaint.region_id != self.request.user.profile.region_id:
                raise Http404

        author = Profile.objects.get(user_id=complaint.user_id)
        context['author'] = author

        managers = Profile.objects.filter(region_id=complaint.region_id, role_id__gte=settings.EXECUTOR)
        context['managers'] = managers

        history = History.objects.filter(complaint_id=complaint.id)
        context['history'] = history

        progress = ScenarioStatus.objects.get(scenario_id=complaint.scenario_id, status_id=complaint.status_id)
        total = ScenarioStatus.objects.filter(scenario_id=complaint.scenario_id).count()
        # progress = 60.*progress.order/total + 8 * complaint.ratio
        progress = 100. * progress.order/total
        context['progress'] = progress

        if self.request.user.profile.role_id >= settings.MANAGER:
            context['can_subscribe'] = True
            context['can_scenario'] = True
            responsible = Responsible.objects.filter(complaint_id=complaint.id)
            context['responsible'] = responsible
            context['scenario_list'] = Scenario.objects.all()

        if self.request.user.profile.role_id >= settings.EXECUTOR:
            context['can_edit'] = True
            context['states'] = ScenarioStatus.objects.filter(scenario_id=complaint.scenario_id).order_by('order')
            context['comment_list'] = Comment.objects.filter(complaint_id=complaint.id)
            context['toggle_visible'] = True
        else:
            context['comment_list'] = Comment.objects.filter(complaint_id=complaint.id, visible=True)
            context['toggle_visible'] = False

        if self.request.user.profile.role_id == settings.EXECUTOR:
            subscribed = Responsible.objects.filter(complaint_id=complaint.id, user_id=self.request.user.id)
            if len(subscribed):
                context['unsubscribe'] = True
            else:
                context['can_edit'] = False

        context['COMPLAINT_CLOSED'] = settings.COMPLAINT_CLOSED
        context['USER'] = settings.USER

        return context


class ComplaintListView(ListView):
    model = Complaint
    context_object_name = 'complaint_list'
    template_name = 'complaint/list.html'
    paginate_by = 10

    def get_queryset(self):
        return Complaint.objects.filter(user_id=self.request.user.id).order_by('-date_created')


class ComplaintManageListView(ListView):
    model = Complaint
    context_object_name = 'complaint_list'
    template_name = 'complaint/executor_list.html'
    paginate_by = 10

    def get_queryset(self):

        sort = self.kwargs.get('sort', 'modfd')
        if sort not in ['modfd', 'cretd']:
            sort = 'modfd'
        mp = {'modfd': '-date_modified', 'cretd': '-date_created'}
        sort = mp[sort]

        if self.request.user.profile.role_id >= settings.MANAGER:
            self.template_name = 'complaint/manager_list.html'
            return Complaint.objects.filter(region_id=self.request.user.profile.region_id).order_by(sort)
        return Complaint.objects.filter(responsible_complaint_key__user_id=self.request.user.id).order_by(sort)

    def get_context_data(self, **kwargs):
        context = super(ComplaintManageListView, self).get_context_data(**kwargs)
        sort = self.kwargs.get('sort', 'modfd')
        if sort not in ['modfd', 'cretd']:
            sort = 'modfd'
        context['sort'] = sort

        return context


class RateView(JSONResponseMixin, FormView):
    form_class = RateForm

    def get(self, request, *args, **kwargs):
        raise Http404

    def get_form_kwargs(self):
        kwargs = super(RateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user

        return kwargs

    def form_valid(self, form):
        complaint = form.save()
        return self.render_to_json_response({'status': 'ok', 'ratio': complaint.ratio},
                                            content_type='application/json')

    def form_invalid(self, form):
        return self.render_to_json_response({'status': 'error'}, content_type='application/json')


def pack(lst):
    names = [_('Not+rated+yet'), _('Very+poor'), _('Poor'), _('Normal'), _('Good'), _('Gorgeous')]
    values, category = [], []
    for i in lst:
        values.append(i[0])
        category.append(names[int(i[1])])

    if len(values) == 0:
        values.append(0)
        category.append(names[0])

    return [values, category]


def pack_city(lst):
    average, rcolor, city = [], [], []
    for i in lst:
        average.append(i[0])
        city.append(i[1])
        ratio = i[0]
        if ratio < 2:
            rcolor.append(RED)
        elif ratio < 3:
            rcolor.append(YELLOW)
        elif ratio < 4:
            rcolor.append(BLUE)
        else:
            rcolor.append(GREEN)

    return [average, rcolor, city]


def region_report(request, region_id, frm, to):
    region = None
    city_list = []
    try:
        region = Region.objects.get(pk=int(region_id))
        city_list = City.objects.filter(region_id=int(region_id)).order_by('name')
    except Region.DoesNotExist:
        raise Exception(_("Region not found"))

    cursor = connection.cursor()
    frm = datetime.strptime(frm, '%d-%m-%Y')
    to = datetime.strptime(to, '%d-%m-%Y')

    query = '''SELECT COUNT(cmpl.id), cmpl.ratio
                      FROM complaint_complaint AS cmpl
                      WHERE cmpl.date_created >= '%s'
                      AND cmpl.date_created <= '%s'
                      AND region_id = %s
                      GROUP BY cmpl.ratio
                   ''' % (frm, to, region_id)

    try:
        cursor.execute(query)
        res = cursor.fetchall()
        values, category = pack(res)
    finally:
        cursor.close()

    cursor = connection.cursor()



    query = '''SELECT AVG(cmpl.ratio), c.name
                FROM complaint_complaint AS cmpl
                LEFT JOIN location_city as c on c.id = cmpl.city_id
                WHERE cmpl.date_created >= '%s'
                AND cmpl.date_created <= '%s'
                AND cmpl.ratio > 0
                AND cmpl.region_id = %s
                GROUP BY cmpl.city_id
            ''' % (frm, to, region_id)

    try:
        cursor.execute(query)
        res = cursor.fetchall()
        average, rcolor, city = pack_city(res)
    finally:
        cursor.close()

    context = RequestContext(request)
    return render_to_response('complaint/report.html', {'values': values,
                                                        'categories': category,
                                                        'average': average,
                                                        'city': city,
                                                        'rcolor': rcolor,
                                                        'colors': ['0000FF'],
                                                        'region': region,
                                                        'from': frm,
                                                        'to': to,
                                                        'city_list': city_list,
                                                        'width': 900,
                                                        'height': 300}, context)


def city_report(request, region_id, city_id, frm, to):

    try:
        Region.objects.get(pk=int(region_id))
    except Region.DoesNotExist:
        raise Exception(_("Region does not exist"))

    try:
        City.objects.get(pk=int(city_id))
    except City.DoesNotExist:
        raise Exception(_("City does not exist"))

    cursor = connection.cursor()
    frm = datetime.strptime(frm, '%d-%m-%Y')
    to = datetime.strptime(to, '%d-%m-%Y')

    query = '''SELECT COUNT(cmpl.id), cmpl.ratio
                      FROM complaint_complaint AS cmpl
                      WHERE cmpl.date_created >= '%s'
                      AND cmpl.date_created <= '%s'
                      AND city_id = %s
                      GROUP BY cmpl.ratio
                   ''' % (frm, to, city_id)

    try:
        cursor.execute(query)
        res = cursor.fetchall()
        values, category = pack(res)
    finally:
        cursor.close()

    context = RequestContext(request)
    return render_to_response('chart.html', {'values': values,
                                             'categories': category,
                                             'colors': ['0000FF'],
                                             'width': 450,
                                             'height': 200}, context)


RED, YELLOW, BLUE, GREENISH, GREEN, GREY = 'F5250A', 'F5AB0A', '0A41F5', '24E3B3', '1DF50A', 'A19EB0'


def pack_regions(lst):
    rname, rcode, rvalue, rcolor = [], [], [], []
    for i in lst:
        ratio = i[0]
        rvalue.append(ratio)
        rname.append(i[1])
        rcode.append(i[2])
        if ratio < 2:
            rcolor.append(RED)
        elif ratio < 3:
            rcolor.append(YELLOW)
        elif ratio < 4:
            rcolor.append(BLUE)
        else:
            rcolor.append(GREEN)

    regions = Region.objects.all()
    for r in regions:
        if r.name not in rname:
            rvalue.append(0)
            rname.append(r.name)
            rcolor.append(GREY)
            rcode.append(r.code)

    return [rname, rcode, rvalue, rcolor]


def kz_report(request, frm, to):
    cursor = connection.cursor()
    frm = datetime.strptime(frm, '%d-%m-%Y')
    to = datetime.strptime(to, '%d-%m-%Y')

    query = '''SELECT AVG(cmpl.ratio), r.name, r.code
                      FROM location_region as r
                      LEFT JOIN  complaint_complaint as cmpl on cmpl.region_id=r.id
                      WHERE cmpl.date_created >= '%s'
                      AND cmpl.date_created <= '%s'
                      AND cmpl.ratio > 0
                      GROUP BY cmpl.region_id
                   ''' % (frm, to)

    try:
        cursor.execute(query)
        res = cursor.fetchall()
        name, code, ratio, color = pack_regions(res)
    finally:
        cursor.close()

    region_list = Region.objects.all()

    context = RequestContext(request)
    return render_to_response('complaint/kz_report.html', {'region_list': region_list,
                                                            'region': name,
                                                             'code': code,
                                                             'value': ratio,
                                                             'color': color,
                                                             'from': frm,
                                                             'to': to,
                                                             'RED': RED,
                                                             'YELLOW': YELLOW,
                                                             'BLUE': BLUE,
                                                             'GREEN' : GREEN,
                                                             'GREY': GREY,
                                                             'from': frm,
                                                             'to': to}, context)