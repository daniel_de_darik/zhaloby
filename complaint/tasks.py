from __future__ import absolute_import
from djcelery import celery
#from django.core.mail import send_mail
#from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from auth.models import User
from status.models import Status
from complaint.models import Complaint
from django.utils.translation import ugettext as _
import requests
from django.conf import settings


@celery.task
def send_complaint_update(user_id, complaint_id, status_id, text):
    try:
        user = User.objects.get(id=int(user_id))

        complaint = Complaint.objects.get(id=int(complaint_id))
        subject = _('Status change on') + '"' + complaint.title + '"'

        status = Status.objects.get(id=int(status_id))

        c = Context({
                    'full_name': user.first_name + " " + user.last_name,
                    'username': user.username,
                    'complaint_id': complaint_id,
                    'title': complaint.title,
                    'status_name': status.name,
                    'manager_text': text,
                    })

        plain_txt = render_to_string('text/complaint_update.txt', c)
        html = render_to_string('text/complaint_update.html', c)
        email = EmailMultiAlternatives(subject, plain_txt)
        email.attach_alternative(html, "text/html")
        # email.to = ['darkhan.imangaliev@gmail.com']
        email.to = [user.email]
        email.send()

        print "Email to " + user.email + " has been successfully sent"
    except Exception as e:
        print "There was an error sending email"
        print e


@celery.task
def send_sms_message(phone, text):

    data = {'user': settings.SMS_USERNAME,
            'pass': settings.SMS_PASSWORD,
            'action': 'post_sms',
            'message': text,
            'target': phone}

    headers = {'content-type': 'application/x-www-form-urlencoded'}
    r = requests.post(settings.SMS_HOST, data=data, headers=headers)

    print "Response from sms service \n" + r.text