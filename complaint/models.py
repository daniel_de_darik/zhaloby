from django.db import models
from status.models import Status, Scenario
from category.models import Category
from location.models import Region, City
from auth.models import User
from datetime import datetime

# Create your models here.


class Complaint(models.Model):
    title = models.CharField(max_length=512, blank=False)
    description = models.TextField(blank=False)
    mobile = models.CharField(max_length=16)
    home_phone = models.CharField(max_length=16)
    date_created = models.DateTimeField(default=datetime.now)
    date_expire = models.DateTimeField(blank=True, null=True)
    date_modified = models.DateTimeField(auto_now=True, default=datetime.now)
    status = models.ForeignKey(Status)
    category = models.ForeignKey(Category)
    region = models.ForeignKey(Region)
    city = models.ForeignKey(City)
    user = models.ForeignKey(User, related_name='user_key')
    scenario = models.ForeignKey(Scenario, related_name='scenario_key', default=1)
    ratio = models.FloatField(default=0)

    def __unicode__(self):
        return "%d %s" % (self.id, self.title)


class History(models.Model):
    date_created = models.DateTimeField(auto_now=True)
    email_text = models.TextField(blank=True, null=True)
    work_text = models.TextField(blank=True, null=True)
    complaint = models.ForeignKey(Complaint, related_name='complaint_key')
    status = models.ForeignKey(Status, related_name='status_key')
    user = models.ForeignKey(User, related_name='user_history_key')


class Responsible(models.Model):
    complaint = models.ForeignKey(Complaint, related_name='responsible_complaint_key')
    user = models.ForeignKey(User, related_name='user_responsible_key')