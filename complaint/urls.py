from django.conf.urls import patterns, url
from complaint import views
from django.contrib.auth.decorators import user_passes_test
from django.conf import settings
from auth.urls import auth_required

head_required = user_passes_test(lambda u: u.profile.role_id >= settings.HEAD, login_url='/auth/denied/')
manager_required = user_passes_test(lambda u: u.profile.role_id >= settings.MANAGER, login_url='/auth/denied/')
executor_required = user_passes_test(lambda u: u.profile.role_id >= settings.EXECUTOR, login_url='/auth/denied/')
user_required = user_passes_test(lambda u: u.profile.role_id >= settings.USER, login_url='/auth/denied/')

urlpatterns = patterns('',
	url(r'^add/$', auth_required(views.ComplaintAddView.as_view()), name='add'),
    url(r'^view/(?P<pk>\d+)/$', auth_required(views.ComplaintDetailView.as_view()), name='view'),
    url(r'^my/(?P<page>\d+)/$', auth_required(views.ComplaintListView.as_view()), name='my'),
    url(r'^manage/(?P<page>\d+)/$', auth_required(executor_required(views.ComplaintManageListView.as_view())), name='manage'),
    url(r'^manage/(?P<sort>\w+)/(?P<page>\d+)/$', auth_required(executor_required(views.ComplaintManageListView.as_view())), name='manage_sort'),
    url(r'^subscribe/$', auth_required(manager_required(views.subscribe)), name='subscribe'),
    url(r'^unsubscribe/$', auth_required(executor_required(views.unsubscribe)), name='unsubscribe'),
    url(r'^update/$', auth_required(executor_required(views.update)), name='update'),
    url(r'^rate/$', auth_required(user_required(views.RateView.as_view())), name='rate'),
    url(r'^report/(?P<region_id>\d+)/(?P<frm>\d{2}-\d{2}-\d{4})/(?P<to>\d{2}-\d{2}-\d{4})/$', manager_required(views.region_report), name='region_report'),
    url(r'^report/(?P<region_id>\d+)/(?P<city_id>\d+)/(?P<frm>\d{2}-\d{2}-\d{4})/(?P<to>\d{2}-\d{2}-\d{4})/$', manager_required(views.city_report), name='city_report'),
    url(r'^report/(?P<frm>\d{2}-\d{2}-\d{4})/(?P<to>\d{2}-\d{2}-\d{4})/$', head_required(views.kz_report), name='rating_kz_report'),
)