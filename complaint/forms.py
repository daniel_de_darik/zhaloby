from django import forms
from location.models import Region, City
from category.models import Category
from complaint.models import Complaint, Responsible, History
from complaint.tasks import send_complaint_update, send_sms_message
from auth.models import Profile
from status.models import Status, Scenario
from django.conf import settings
from django.utils.translation import ugettext as _
from datetime import datetime
import re


class ComplaintForm(forms.ModelForm):

    class Meta:
        model = Complaint
        fields = ['title', 'description', 'mobile', 'home_phone', 'category_id', 'region_id', 'city_id']

    title = forms.CharField(max_length=256)
    description = forms.CharField(widget=forms.Textarea)
    mobile = forms.CharField(max_length=16)
    home_phone = forms.CharField(max_length=16)
    category_id = forms.IntegerField()
    region_id = forms.IntegerField()
    city_id = forms.IntegerField()

    def clean_title(self):
        title = self.cleaned_data.get('title', '')
        if len(title.split()) < 1:
            raise forms.ValidationError(_('Too short header'))

        return title

    def clean_description(self):
        description = self.cleaned_data.get('description', '')
        if len(description.split()) < 2:
            raise forms.ValidationError(_('Too short description'))

        return description

    def clean_mobile(self):
        mobile = self.cleaned_data.get('mobile', '')
        match = re.match(r'\+7(\d){10}', mobile)
        if match:
            return mobile
        raise forms.ValidationError(_('Enter valid mobile phone number'))

    def clean_home_phone(self):
        home_phone = self.cleaned_data.get('home_phone', '')
        match = re.match(r'(\d){7}', home_phone)

        if match:
            return home_phone
        raise forms.ValidationError(_('Enter valid home phone number'))

    def clean_region_id(self):
        region_id = self.cleaned_data.get('region_id', 0)
        try:
            Region.objects.get(id=int(region_id))
            return region_id
        except Region.DoesNotExist:
            raise forms.ValidationError(_('Chose a region please'))

    def clean_city_id(self):
        city_id = self.cleaned_data.get('city_id', 0)
        try:
            City.objects.get(id=int(city_id))
            return city_id
        except City.DoesNotExist:
            raise forms.ValidationError(_('Chose a city please'))

    def clean_category_id(self):
        category_id = self.cleaned_data.get('category_id', 0)
        try:
            category = Category.objects.get(id=int(category_id))
            # print category.id, category.name, category.parent_id
            if category.parent_id is not None:
                return category_id
            raise forms.ValidationError(_('Chose a deeper category please'))
        except Category.DoesNotExist:
            raise forms.ValidationError(_('Chose a category please'))

    def save(self, **kwargs):

        user = kwargs.get('user', None)
        mobile = self.cleaned_data.get('mobile', '')
        home_phone = self.cleaned_data.get('home_phone', '')

        profile = Profile.objects.get(user_id=user.id)
        changed = False

        if not profile.mobile or profile.mobile != mobile:
            profile.mobile = mobile
            changed = True
        if not profile.home_phone or profile.home_phone != home_phone:
            profile.home_phone = home_phone
            changed = True

        if changed:
            profile.save()

        complaint = Complaint(user_id=user.id,
                              region_id=self.cleaned_data.get('region_id', -1),
                              city_id=self.cleaned_data.get('city_id', -1),
                              title=self.cleaned_data.get('title', ''),
                              description=self.cleaned_data.get('description', ''),
                              category_id=self.cleaned_data.get('category_id', -1),
                              mobile=mobile,
                              home_phone=home_phone,
                              status_id=1)

        complaint.save()

        history = History(complaint_id=complaint.id,
                          status_id=1,
                          work_text=_('New complaint on service'),
                          email_text='',
                          user_id=user.id)
        history.save()

        # send_sms_message.delay(mobile, _('Your complaint has been added to our database. Thank you'))

        return complaint


class ComplaintUpdateForm(forms.Form):

    def __init__(self, user, *args, **kwargs):
        super(ComplaintUpdateForm, self).__init__(*args, **kwargs)
        self.complaint = None
        self.status = None
        self.user = user

    complaint_id = forms.IntegerField(required=True)
    status_id = forms.IntegerField(required=True)
    scenario_id = forms.IntegerField(required=False)
    date_expire = forms.DateField(input_formats=['%d/%m/%Y'])
    email_text = forms.CharField(widget=forms.Textarea, required=False)
    work_text = forms.CharField(widget=forms.Textarea, required=False)

    def clean_complaint_id(self):
        complaint_id = self.cleaned_data.get('complaint_id', 0)
        try:
            self.complaint = Complaint.objects.get(id=int(complaint_id))

            record = Responsible.objects.filter(complaint_id=self.complaint.id, user_id=self.user.id)
            if len(record) == 0:
                if self.user.profile.role_id <= settings.EXECUTOR:
                    raise forms.ValidationError(_('Not enough right to perform this action'))

            return complaint_id
        except Complaint.DoesNotExist:
            raise forms.ValidationError(_("Complaint doesn't exist"))

    def clean_status_id(self):
        status_id = self.cleaned_data.get('status_id', 0)
        try:
            self.status = Status.objects.get(id=int(status_id))
            return status_id
        except Status.DoesNotExist:
            raise forms.ValidationError(_("Status doesn't exist"))

    def clean_scenario_id(self):
        if self.user.profile.role_id < settings.MANAGER:
            return None
        scenario_id = self.cleaned_data.get('scenario_id', 0)
        try:
            Scenario.objects.get(id=int(scenario_id))
            return scenario_id
        except Status.DoesNotExist:
            raise forms.ValidationError(_("Scenario doesn't exist"))

    # def clean_email_text(self):
    #     pass
    #
    # def clean_work_text(self):
    #     pass

    def save(self):

        email_text = self.cleaned_data.get('email_text', '')

        history = History(complaint_id=self.cleaned_data.get('complaint_id', 0),
                          status_id=self.cleaned_data.get('status_id', 0),
                          email_text=email_text,
                          work_text=self.cleaned_data.get('work_text', ''),
                          user_id=self.user.id
                          )
        history.save()

        date_expire = self.cleaned_data.get('date_expire', '')
        self.complaint.date_expire = date_expire
        self.complaint.status_id = self.cleaned_data.get('status_id', 0)
        self.complaint.date_modified = datetime.now()
        if self.user.profile.role_id >= settings.MANAGER:
            self.complaint.scenario_id = self.cleaned_data.get('scenario_id', 0)

        self.complaint.save()

        if email_text:
            send_complaint_update.delay(self.complaint.user_id, self.complaint.id, self.status.id, email_text)

        return self.complaint


class SubscribeForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(SubscribeForm, self).__init__(*args, **kwargs)
        self.complaint = None
        self.profile = None

    complaint_id = forms.IntegerField(required=True)
    manager_id = forms.IntegerField(required=True)

    def clean_complaint_id(self):
        complaint_id = self.cleaned_data.get('complaint_id', 0)
        try:
            self.complaint = Complaint.objects.get(id=int(complaint_id))
            return complaint_id
        except Complaint.DoesNotExist:
            raise forms.ValidationError(_('Complaint does not exist'))

    def clean_manager_id(self):
        manager_id = self.cleaned_data.get('manager_id', 0)
        try:
            self.profile = Profile.objects.get(user_id=int(manager_id))
            if self.complaint.region_id != self.profile.region_id:
                raise forms.ValidationError(_('Different regions not allowed'))

            if self.profile.role_id < settings.EXECUTOR:
                raise forms.ValidationError(_('Too low rights to perform the action'))

            return manager_id
        except Profile.DoesNotExist:
            raise forms.ValidationError(_('Profile does not exist'))

    def save(self):
        complaint_id = self.cleaned_data.get('complaint_id', 0)
        user_id = self.cleaned_data.get('manager_id', 0)

        record = Responsible.objects.filter(complaint_id=complaint_id, user_id=user_id)

        if len(record):
            record.delete()
        else:
            record = Responsible(complaint_id=complaint_id, user_id=user_id)
            record.save()

        return self.complaint


class RateForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.complaint = None
        super(RateForm, self).__init__(*args, **kwargs)

    complaint_id = forms.IntegerField(required=True)
    ratio = forms.IntegerField(required=True, min_value=1, max_value=5)

    def clean_complaint_id(self):
        complaint_id = self.cleaned_data.get('complaint_id', 0)
        try:
            self.complaint = Complaint.objects.get(id=int(complaint_id))
            if self.user.id != self.complaint.user_id:
                raise forms.ValidationError(_("Don't have permission to rate"))
            return complaint_id
        except Complaint.DoesNotExist:
            raise forms.ValidationError(_('Complaint does not exist'))

    def save(self):
        ratio = self.cleaned_data.get('ratio', 1)

        self.complaint.ratio = ratio
        self.complaint.save()

        return self.complaint