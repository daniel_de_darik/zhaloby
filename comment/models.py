from django.db import models
from datetime import datetime
from complaint.models import Complaint
from auth.models import User
# Create your models here.


class Comment(models.Model):
    text = models.TextField()
    date_created = models.DateTimeField(default=datetime.now)
    visible = models.BooleanField(default=True)
    complaint = models.ForeignKey(Complaint)
    user = models.ForeignKey(User)