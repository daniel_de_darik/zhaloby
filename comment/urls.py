from django.conf.urls import patterns, url
from comment import views
from auth.urls import auth_required

urlpatterns = patterns('',
	url(r'^add/$', auth_required(views.CommentAddView.as_view()), name='add'),
)