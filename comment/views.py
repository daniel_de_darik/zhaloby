from django.views.generic.edit import CreateView
from comment.models import Comment
from comment.forms import CommentAddForm
from django.http import HttpResponse, Http404
from django.template.loader import render_to_string
import json
# Create your views here.


class JSONResponseMixin(object):
    def render_to_json_response(self, context, **kwargs):
        return HttpResponse(json.dumps(self.get_data(context)), **kwargs)

    def get_data(self, context):
        return context


class CommentAddView(JSONResponseMixin, CreateView):
    model = Comment
    form_class = CommentAddForm

    def get(self, request, *args, **kwargs):
        raise Http404

    def get_form_kwargs(self):
        kwargs = super(CommentAddView, self).get_form_kwargs()
        kwargs['user'] = self.request.user

        return kwargs

    def form_invalid(self, form):
        return self.render_to_json_response({'status': 'error'}, content_type='application/json')

    def form_valid(self, form):
        comment = form.save()
        response = {'status': 'ok',
                    'template': render_to_string('comment/comment.html', {'comment': comment})}

        return self.render_to_json_response(response, content_type='application/json')