from django import forms
from complaint.models import Complaint
from comment.models import Comment
from django.utils.translation import ugettext as _
from datetime import datetime
from django.conf import settings


class CommentAddForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['text', 'complaint_id', 'visible']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(CommentAddForm, self).__init__(*args, **kwargs)

    text = forms.CharField(widget=forms.Textarea)
    complaint_id = forms.IntegerField()
    visible = forms.BooleanField(required=False)

    def clean_text(self):
        text = self.cleaned_data.get('text', '')
        if len(text.split()) == 0:
            raise forms.ValidationError(_('Too short description'))

        return text

    def clean_complaint_id(self):
        complaint_id = self.cleaned_data.get('complaint_id', 0)
        try:
            complaint = Complaint.objects.get(id=int(complaint_id))
            if self.user.profile.role_id < settings.EXECUTOR:
                if complaint.user_id != self.user.id:
                    forms.ValidationError(_('Not allowed to comment'))

            if self.user.profile.region_id != complaint.region_id:
                forms.ValidationError(_('Not allowed to comment'))

            return complaint_id
        except Complaint.DoesNotExist:
            raise forms.ValidationError(_('Complaint does not exist'))

    def save(self):
        visible = self.cleaned_data.get('visible', True)
        complaint_id = self.cleaned_data.get('complaint_id', 0)
        comment = Comment(complaint_id=complaint_id,
                          text=self.cleaned_data.get('text', ''),
                          user_id=self.user.id,
                          visible=visible
                          )

        comment.save()

        return comment
